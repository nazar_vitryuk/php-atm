<!DOCTYPE html>
<html>
<head>
<title>Первый сайт на PHP</title>
<meta charset="utf-8">
</head>
<body>

<?php

$money = array(
    new Banknote(500, 15),
    new Banknote(200, 10),
    new Banknote(100, 14),
    new Banknote(50, 24),
    new Banknote(20, 8),
    new Banknote(10, 10),
    new Banknote(5, 14)

);
$summ  = $_POST["summ"];

class Banknote
{
    
    public $name;
    
    public function __construct($nominal, $summ)
    {
        $this->nominal = $nominal;
        $this->summ    = $summ;
    }
}

function calcAllMoney()
{
    $allMoney = 0;
    global $money;
    
    foreach ($money as $banknote)
        $allMoney += $banknote->nominal * $banknote->summ;
    
    return $allMoney;
}

function giveMoney()
{
    $operationStatus = false;
    $givedAmount = [];
    $givedSum;
    global $money;
    global $summ;
    if (calcAllMoney() > $summ) {
    	if (is_int($summ / 5)) {
    		echo "---GET YOUR MONEY---";
        foreach ($money as $nominal) {
            if ($operationStatus) {
                break;
            } else {
                for ($i = 0; $i < $nominal->summ; $i++) {
                    if ($summ == 0 || $summ < 0) {
                        $operationStatus = true;
                        break;
                    } else {
                        $summRest = $summ - $nominal->nominal;
                        if ($summRest < 0) {
                            break;
                        } else {
                            $summ -= $nominal->nominal;
                            $givedAmount = array(500 => $nominal);
                            $givedSum =+ $nominal->nominal;
                        }
                    }
                }
            }
        }
        echo "--------Operation Status--------------";
        echo "<p>You get: ".$givedSum."</p>";
        echo "The amount of notes: ";
        foreach ($givedAmount as $note) {
        	echo "<p>".$note."</p>";
        }
        if ($summ > 0) {
        	echo "Your card balance:".$summ;
        }
    	} else {
    		echo "<h3>The amount you paste can't be given on this ATM:</h3>";
    		echo "<p>Amount you have entered is not multiple to ".end($money)->nominal."</p>";
    		echo "<a href='/'>Enter another amount</a>";
    	}
    } else {
    	echo "<h2>ATM doesn't have enough money for this operation.</h2>";
        echo "The amount you want: ".$summ;
        echo "<p>Recomended amount: ".calcAllMoney()."</p>";
        echo "<button><a href='/'>Enter another amount</a></button>";
    }
}

giveMoney();



?>

</body>
</html>